package it.unibo.oop.lab.mvc;

import java.util.ArrayList;
import java.util.List;

public class ControllerImpl implements Controller {
    private final List<String> strings;

    public ControllerImpl() {
        strings = new ArrayList<>();
    }

    /**
     * set the next string to print.
     * 
     * @string next string to print
     */
    public void setNextString(final String string) {
        if (string == null) {
            throw new IllegalArgumentException();
        }
        strings.add(string);
    }

    /**
     * get the next string to print.
     * 
     * @return the next string to print
     */
    public String getNextString() {
        return strings.get(strings.size() - 1);
    }

    /**
     * get the history of the printed string.
     * 
     * @return them in form of List<String>
     */
    public List<String> getPrintedStrings() {
        return new ArrayList<String>(strings);
    }

    /**
     * print the current string on the standard output.
     */
    public void printString() {
        System.out.println(strings.get(strings.size() - 1));
    }

}
