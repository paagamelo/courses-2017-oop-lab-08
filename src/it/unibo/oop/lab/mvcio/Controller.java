package it.unibo.oop.lab.mvcio;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * 
 */
public class Controller {
    private File file;

    /**
     * Constructor.
     * 
     * @throws IOException
     *             if the file cannot be opened
     */
    public Controller() throws IOException {
        this.file = new File(System.getProperty("user.home") + System.getProperty("file.separator") + "output.dat");
    }

    /*
     * This class must implement a simple controller responsible of I/O access. It
     * considers a single file at a time, and it is able to serialize objects in it.
     * 
     * Implement this class in such a way that:
     * 
     * 1) It has a method for setting a File as current file
     * 
     * 2) It has a method for getting the current File
     * 
     * 3) It has a method for getting the path (in form of String) of the current
     * File
     * 
     * 4) It has a method that gets a Serializable as input and saves such Object in
     * the current file. Remember how to use the ObjectOutputStream. This method may
     * throw IOException.
     * 
     * 5) By default, the current file is "output.dat" inside the user home folder.
     * A String representing the local user home folder can be accessed using
     * System.getProperty("user.home"). The separator symbol (/ on *nix, \ on
     * Windows) can be obtained as String through the method
     * System.getProperty("file.separator"). The combined use of those methods leads
     * to a software that run correctly on every platform.
     */
    /**
     * Set the file as the current one.
     * 
     * @param file
     *            file to set
     */
    public void setFile(final File file) {
        this.file = file;
    }

    /**
     * Get the current file.
     * 
     * @return the current file
     */
    public File getFile() {
        return this.file;
    }

    /**
     * Get the path of the current file in String form.
     * 
     * @return a string containing the path of the file
     */
    public String getPath() {
        return file.getPath();
    }

    /**
     * Write an object on the file.
     * 
     * @param object
     *            the object to write
     * @throws IOException
     *             if the file cannot be wrote
     */
    public void writeObject(final Class<? extends Serializable> object) throws IOException {
        try (ObjectOutputStream ostream = new ObjectOutputStream(
                new BufferedOutputStream(new FileOutputStream(this.file)))) {
            ostream.writeObject(object);
            ostream.close();
        }
    }
}
