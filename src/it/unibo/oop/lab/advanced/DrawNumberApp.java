package it.unibo.oop.lab.advanced;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 */
public class DrawNumberApp implements DrawNumberViewObserver {

    private static int min;
    private static int max;
    private static int attempts;
    private final DrawNumber model;
    private final List<DrawNumberView> views;

    /**
     * 
     */
    public DrawNumberApp() {
        loadSettings();
        this.model = new DrawNumberImpl(min, max, attempts);
        this.views = new ArrayList<>(Arrays.asList(new DrawNumberViewImpl(), new DrawNumberViewImpl(), new MatchLogViewFileImpl(), new MatchLogViewOutputImpl()));
        for (final DrawNumberView view : views) {
            view.setObserver(this);
            view.start();
        }
    }

    @Override
    public void newAttempt(final int n) {
        try {
            final DrawResult result = model.attempt(n);
            for (final DrawNumberView view : views) {
                view.result(result);
            }
        } catch (IllegalArgumentException e) {
            for (final DrawNumberView view : views) {
                view.numberIncorrect();
            }
        } catch (AttemptsLimitReachedException e) {
            for (final DrawNumberView view : views) {
                view.limitsReached();
            }
        }
    }

    @Override
    public void resetGame() {
        this.model.reset();
    }

    @Override
    public void quit() {
        System.exit(0);
    }

    private void loadSettings() {
        Settings.load();
        min = Settings.getMIN();
        max = Settings.getMAX();
        attempts = Settings.getATTEMPTS();
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        new DrawNumberApp();
    }

}
