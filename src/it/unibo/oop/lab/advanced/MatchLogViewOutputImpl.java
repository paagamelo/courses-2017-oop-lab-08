package it.unibo.oop.lab.advanced;
/**
 * This class implements a new view for the App: it prints your Match Log on the stdout.
 */
public class MatchLogViewOutputImpl extends MatchLogView {

    /**
     * @param message
     *              the message to print on the stdout
     */
    protected void print(final String message) {
        System.out.println(message);
    }

}
