package it.unibo.oop.lab.advanced;

/**
 * This abstract class implements a new view for the App: it prints your Match Log.
 * The Match Log can be printed in different files, this depend on how different classes implements the abstract method "print"
 * 
 */
abstract class MatchLogView implements DrawNumberView {

    private static final String INCORRECT_NUMBER_STRING = "Incorrect Number..";
    private static final String LIMITS_REACHED_STRING = "You LOSE!";

    private DrawNumberViewObserver observer;

    @Override
    public void setObserver(final DrawNumberViewObserver observer) {
        this.observer = observer;
    }

    /**
     */
    public void start() {
        print("Match starts!");
    }

    /**
     * 
     */
    public void numberIncorrect() {
        print(INCORRECT_NUMBER_STRING);
    }

    /**
     * @param res
     *          the result of the attempt
     */
    public void result(final DrawResult res) {
        switch (res) {
        case YOURS_HIGH:
            print("Too High!");
            return;
        case YOURS_LOW:
            print("Too Low!");
            return;
        case YOU_WON:
            print("You Won");
            break;
        default:
            throw new IllegalStateException("Unexpected result: " + res);
        }
        observer.resetGame();
    }

    /**
     * 
     */
    public void limitsReached() {
        print(LIMITS_REACHED_STRING);
    }

    /**
     * @param message
     *              the error message to print
     */
    public void displayError(final String message) {
        print(message);
    }
    
    /**
     * The file on which the Match Log will be print depends on how this method is implemented in different classes.
     * 
     * @param message
     *              the message to print
     */
    protected abstract void print(final String message);

}
