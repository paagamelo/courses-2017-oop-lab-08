package it.unibo.oop.lab.advanced;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * This class implements a new view for the app: it prints your Match Log on a file named DNAppMatchLog.txt.
 * 
 * The file can be found in your own home folder
 */
public class MatchLogViewFileImpl extends MatchLogView {

    private static final String OPEN_ERROR_STRING = "Something gone wrong, couldn't open MatchLog file!";
    private static final String PATH = System.getProperty("user.home") + System.getProperty("file.separator") + "DNAppMatchLog.txt";

    @Override
    /**
     * Tries to open "DNAppMatchLog" file, if the file does not exist it creates one.
     */
    public void start() {
        final File f = new File(PATH);
        if (!f.exists()) {
            try {
                Files.createFile(Paths.get(PATH));
            } catch (IOException e) {
                System.out.println(OPEN_ERROR_STRING);
            }
        }
        print("Match starts!");
    }

    /**
     * Tries to open an Output stream, if it fails print a message on the stdout.
     * Otherwise print the message on the file
     *
     */
    protected void print(final String message) {
        try (PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(new File(PATH), true)))) {
            pw.println(message);
        } catch (IOException e) {
            System.out.println(OPEN_ERROR_STRING);
        }
    }
}
