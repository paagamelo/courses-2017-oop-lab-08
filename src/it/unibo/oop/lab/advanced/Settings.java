package it.unibo.oop.lab.advanced;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.StringTokenizer;

/**
 * This class loads the Settings for the game.
 * 
 * It is a utility class
 * 
 * The class tries to load your own settings file, if the file does not exist it is created copying the default settings file
 * The setting file can be found in your home folder with the name ".DrawNumberApp.yml", the default setting file can be found in resource folder
 * 
 */
public final class Settings {

    private static final String PATH = System.getProperty("user.home") + System.getProperty("file.separator") + ".DrawNumberApp.yml";

    private static int min;
    private static int max;
    private static int attempts;

    private Settings() {
    }
    /**
     * @return the Settings value of MIN.
     */
    public static int getMIN() {
        return Settings.min; 
    }
    /**
     * @return the Settings value of MAX.
     */
    public static int getMAX() {
        return Settings.max; 
    }
    /**
     * @return the Settings value of ATTEMPTS.
     */
    public static int getATTEMPTS() {
        return Settings.attempts; 
    }
    /**
     * loads the setting file.
     */
    public static void load() {
        readSettings(loadFile());
    }

    /*
     * Checks if the file exist in your home folder, if not it creates one
     */
    private static File loadFile() {
        final File settingsFile = new File(PATH);
        if (!settingsFile.exists()) {
            createFile();
        }
        return settingsFile;
    }

    /*
     * Creates a new settings file in your home folder, copying the default settings file in the resource folder
     */
    private static void createFile() {
        try {
            final String sourceName = Settings.class.getResource("/config.yml").getPath();
            Files.copy(Paths.get(sourceName), Paths.get(PATH));
        } catch (IOException e) {
            System.out.println("Settings file is missing!");
        }
    }

    /*
     * Reads each setting from the file
     * 
     * This version of the Settings reader suppose to know the order with which the settings are written in the file
     * if the user manage the setting file somehow wrong, the result is unpredictable
     */
    private static void readSettings(final File settings) {
        try (BufferedReader reader = new BufferedReader(new FileReader(settings))) {
            min = getValue(reader);
            max = getValue(reader);
            attempts = getValue(reader);
        } catch (IOException e) {
            System.out.println("Couldn't read settings file!");
            e.printStackTrace();
        }
    }

    /*
     * Reads each line from the file and deals with cutting the useless
     */
    private static int getValue(final BufferedReader reader) throws IOException {
        final String line = reader.readLine();
        final StringTokenizer st = new StringTokenizer(Objects.requireNonNull(line));
        st.nextToken();
        return Integer.parseInt(st.nextToken());
    }

}
